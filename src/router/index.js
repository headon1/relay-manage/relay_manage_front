import Vue from "vue";
import VueRouter from "vue-router";
import Main from "../views/Main.vue";
import workTemplates from "../views/workTemplates.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Main",
    component: Main,
  },
  {
    path: "/work-templates",
    name: "workTemplates",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: workTemplates,
  },
];

const router = new VueRouter({
  routes,
});

export default router;
