import axios from "axios";

import { api_base_url } from "../../config";

const state = {
  tasks: [],
  filters: { domains: [] },
};

const getters = {
  allTasks: (state) => state.tasks,
  getTask: (state) => (id) => state.tasks.find((task) => task.id == id),
  filterTasks: (state) => {
    return state.tasks.filter((task) =>
      state.filters.domains.includes(task.domain_id)
    );
  },
  GetUnscheduleTasks: (state, getters) =>
    getters.filterTasks.filter((task) => task.start == null),
  filters: (state) => state.filters,
};

const actions = {
  fetchAllTasks({ commit }) {
    axios.get(api_base_url + "task/all").then(
      (response) => {
        commit("setTasks", response.data);
      },
      (error) => {
        window.alert(error);
      }
    );
  },
  updateTask({ commit }, updTask) {
    axios.put(api_base_url + "task", updTask).then(
      (response) => {
        commit("updateTask", response.data);
        return response.data;
      },
      (error) => {
        window.alert(error);
      }
    );
  },
  createTask({ commit }, task) {
    return axios.post(api_base_url + "task", task).then(
      (response) => {
        commit("appendTask", response.data);
        return response.data;
      },
      (error) => {
        window.alert(error);
      }
    );
  },
  deleteTask({ commit }, id) {
    axios.delete(api_base_url + `task/${id}`).then(
      () => {
        commit("deleteTask", id);
      },
      (error) => {
        window.alert(error);
      }
    );
  },
};

const mutations = {
  setTasks: (state, tasks) => (state.tasks = tasks),
  updateTask: (state, updateTask) => {
    const index = state.tasks.findIndex((task) => task.id === updateTask.id);
    if (index !== -1) {
      state.tasks.splice(index, 1, updateTask);
    }
  },
  appendTask: (state, task) => state.tasks.push(task),
  deleteTask: (state, id) =>
    (state.tasks = state.tasks.filter((task) => task.id !== id)),
  setFilters: (state, payload) => {
    for (const filter in payload) {
      state.filters[filter] = payload[filter];
    }
  },
};

export default {
  state,
  actions,
  getters,
  mutations,
};
