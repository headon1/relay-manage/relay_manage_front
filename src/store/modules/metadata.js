import axios from "axios";
import { api_base_url } from "../../config";

const state = {
  domains: [],
  taskTypes: [],
  urgencies: [],
  statuses: [],
};

const getters = {
  domains: (state) => state.domains,
  taskTypes: (state) => state.taskTypes,
  urgencies: (state) => state.urgencies,
  statuses: (state) => state.statuses,
};

const actions = {
  async fetchMetadata({ commit }) {
    const domainsResponse = axios.get(api_base_url + "metadata/domains");
    const taskTypesNamesResponse = axios.get(
      api_base_url + "metadata/task-types"
    );
    const urgenciesResponse = axios.get(api_base_url + "metadata/urgencies");
    const statusesResponse = axios.get(api_base_url + "metadata/statuses");

    const domains = await domainsResponse;
    const taskTypes = await taskTypesNamesResponse;
    const urgencies = await urgenciesResponse;
    const statuses = await statusesResponse;

    commit("setDomains", domains.data);
    commit("setTaskTypes", taskTypes.data);
    commit("setUrgencies", urgencies.data);
    commit("setStatuses", statuses.data);
    commit(
      "setFilters",
      { domains: domains.data.map((obj) => obj.id) },
      { root: true }
    );
  },
};

const mutations = {
  setDomains: (state, domains) => (state.domains = domains),
  setTaskTypes: (state, taskTypes) => (state.taskTypes = taskTypes),
  setUrgencies: (state, urgencies) => (state.urgencies = urgencies),
  setStatuses: (state, statuses) => (state.statuses = statuses),
};

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations,
};
