import Vue from "vue";
import Vuex from "vuex";
import tasks from "./modules/tasks";
import metadata from "./modules/metadata";
// import fatherTaskDialog from "./modules/fatherTaskDialog";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: { tasks, metadata },
});
