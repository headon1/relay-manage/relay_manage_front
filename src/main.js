import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import VueConfirmDialog from "vue-confirm-dialog";
import { ValidationProvider } from "vee-validate";

Vue.config.productionTip = false;
Vue.use(VueConfirmDialog);
Vue.component("vue-confirm-dialog", VueConfirmDialog.default);
Vue.component("ValidationProvider", ValidationProvider);

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
