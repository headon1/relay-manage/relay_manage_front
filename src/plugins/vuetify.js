import Vue from "vue";
import Vuetify from "vuetify/lib";
import DatetimePicker from "vuetify-datetime-picker";

Vue.use(Vuetify);
Vue.use(DatetimePicker);

export default new Vuetify({
  rtl: true,
  icons: {
    iconfont: "mdiSvg", // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4' || 'faSvg'
  },
});
